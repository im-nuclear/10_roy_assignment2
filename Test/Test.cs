﻿using NUnit.Framework;
using System;
using _Roy_Assignment2;
namespace Test
{
    [TestFixture()]
    public class Test
    {
        Game game;
        [SetUp]
        public void GameSetUp()
        {
            game = new Game();
        }
        public void RollMany(int Rolls, int Pins)
        {
            for (int i = 0; i < Rolls; i++)
            {
                game.Roll(Pins);
            }
        }

        [Test]
        public void GutterGame()
        {
            RollMany(20, 0);
            Assert.That(game.Score, Is.EqualTo(0));
        }

        [Test]
        public void RollOnes()
        {
            RollMany(20, 1);
            Assert.That(game.Score, Is.EqualTo(20));
        }

        [Test]
        public void RollFirstFrameSpare()
        {
            game.Roll(8); game.Roll(1);
            RollMany(18, 1);
            Assert.That(game.Score, Is.EqualTo(27));
        }

        [Test]
        public void RollFirstFrameStrike()
        {
            game.Roll(10);
            RollMany(18, 1);
            Assert.That(game.Score, Is.EqualTo(30));
        }

        [Test]
        public void PerfectGame()
        {
            RollMany(12, 10);
            Assert.That(game.Score, Is.EqualTo(300));
        }

        [Test]
        public void LastFrameStrike()
        {
            RollMany(18, 1);
            RollMany(3, 10);
            Assert.That(game.Score, Is.EqualTo(48));
        }

        [Test]
        public void AllSpares()
        {
            RollMany(20, 5);
            Assert.That(game.Score, Is.EqualTo(150));
        }

        [Test]
        public void TypicalGame()
        {
            game.Roll(7); game.Roll(2);
            game.Roll(10);
            game.Roll(5); game.Roll(3);
            game.Roll(3); game.Roll(6);
            game.Roll(10);
            game.Roll(10);
            game.Roll(8); game.Roll(2);
            game.Roll(7); game.Roll(1);
            game.Roll(4); game.Roll(3);
            game.Roll(6); game.Roll(4);
            Assert.That(game.Score, Is.EqualTo(135));
        }
    }
}
