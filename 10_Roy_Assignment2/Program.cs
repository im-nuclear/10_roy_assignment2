﻿using System;

namespace _Roy_Assignment2
{
    public class Game
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }


        int[] pinFalls = new int[21];
        int rollCounter;

        public void Roll(int pins)
        {
            pinFalls[rollCounter] = pins;
            rollCounter++;
        }

        public bool IsStrike(int i)
        {
            return pinFalls[i] == 10;
        }
        public bool IsSpare (int i)
        {
            return pinFalls[i] + pinFalls[i + 2] == 10;
        }
        public int IsStrikeBonus(int i)
        {
            return pinFalls[i + 1] + pinFalls[i + 2];
        }
        public int IsSpareBonus(int i)
        {
            return pinFalls[i + 2];
        }


        public int Score()
        {
            int score = 0;
            int i = 0;

            for (int frame = 0; frame < 10; frame++)
            {
                if (IsStrike(i))
                {
                    score += 10 + IsStrikeBonus(i);
                    i += 1;
                }
                else if (IsSpare(i))
                {
                    score += 10 + IsSpareBonus(i);
                    i += 1;
                }
                else
                {
                    score += pinFalls[i] + pinFalls[i + 1];
                    i += 2;
                }
            }
            return score;
        }
    }
}
